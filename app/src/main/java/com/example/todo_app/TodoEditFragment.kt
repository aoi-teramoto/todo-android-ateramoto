package com.example.todo_app

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_todo_edit.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class TodoEditFragment : Fragment() {

    private val args: TodoEditFragmentArgs by navArgs()
    private val sendDateFormat = SimpleDateFormat(DATE_FORMAT_FOR_SERVER, Locale.getDefault())
    private val dateFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button_register.isEnabled = false
        setupEditCharacter()

        val todo = args.Todo
        if (todo != null) {
            setUpTodoText(todo)
        }
        setUpButtonText(todo?.id)

        text_date_content.setOnClickListener {
            showDatePicker()
        }

        button_register.setOnClickListener {
            CoroutineScope(Default).launch {
                sendTodo(todo?.id)
            }
        }
    }

    private fun setUpTodoText(todo: TodoModel) {
        edit_title.setText(todo.title)
        todo.detail?.let {
            edit_detail.setText(it)
        }
        todo.date?.let {
            text_date_content.text = dateFormat.format(it)
        }
    }

    private fun setUpButtonText(id: Int?) {
        val text = if (id == null) R.string.button_register else R.string.button_update
        button_register.setText(text)
    }

    private fun updateTitleCounterTextColor() {
        if (edit_title.length() > TITLE_MAX_LENGTH) {
            text_title_length.setTextColor(Color.RED)
        } else {
            text_title_length.setTextColor(Color.GRAY)
        }
    }

    private fun updateDetailCounterTextColor() {
        if (edit_detail.length() > DETAIL_MAX_LENGTH) {
            text_detail_length.setTextColor(Color.RED)
        } else {
            text_detail_length.setTextColor(Color.GRAY)
        }
    }

    private fun updateRegisterButtonState() {
        button_register.isEnabled =
            edit_title.length() in 1..TITLE_MAX_LENGTH && edit_detail.length() <= DETAIL_MAX_LENGTH
    }

    private fun setupEditCharacter() {
        edit_title.addTextChangedListener {
            text_title_length.text = edit_title.length().toString()
            updateTitleCounterTextColor()
            updateRegisterButtonState()
        }

        edit_detail.addTextChangedListener {
            text_detail_length.text = edit_detail.length().toString()
            updateDetailCounterTextColor()
            updateRegisterButtonState()
        }
    }

    private fun showDatePicker() {
        val calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN)
        val datePickerDialog = DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener { _, year, month, day ->
                text_date_content.text = ("$year/${month + 1}/$day")
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.setButton(
            DialogInterface.BUTTON_NEUTRAL,
            getString(R.string.button_date_delete)
        ) { _, _ ->
            text_date_content.text = ""
        }
        datePickerDialog.show()
    }

    private suspend fun sendTodo(id: Int?) {
        try {
            val apiRequest = ApiClient().apiRequest
            val requestBody = makeTodoRequestBody()
            val result = if (id == null) {
                apiRequest.postTodo(requestBody).execute()
            } else {
                apiRequest.updateTodo(id, requestBody).execute()
            }
            withContext(Main) {
                if (result.isSuccessful) {
                    onSuccess(id != null)
                } else {
                    val gson = GsonUtils.generateGson()
                    val body =
                        gson.fromJson(result.errorBody()?.string(), CommonResponse::class.java)
                    showErrorAlertDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorAlertDialog(getString(R.string.exception_error_alert_message))
            }
        }
    }

    private fun makeTodoRequestBody(): TodoRequestBody {
        val title = edit_title.text.toString()
        val detail = edit_detail.text.toString()
        val dateText = text_date_content.text
        val date = if (dateText.isBlank()) {
            ""
        } else {
            val parsedDate = dateFormat.parse(dateText.toString())
            sendDateFormat.format(parsedDate)
        }

        return TodoRequestBody(title, detail, date)
    }

    private fun onSuccess(isUpdate: Boolean) {
        val message =
            if (isUpdate) R.string.snackbar_update_text else R.string.snackbar_register_text

        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.value =
            Event(getString(message))
        findNavController().navigateUp()
    }

    companion object {
        private const val TITLE_MAX_LENGTH = 100
        private const val DETAIL_MAX_LENGTH = 1000
        private const val DATE_FORMAT = "yyyy/M/d"
        private const val DATE_FORMAT_FOR_SERVER = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
}
