package com.example.todo_app

import android.app.AlertDialog
import androidx.fragment.app.Fragment

fun Fragment.showErrorAlertDialog(errorMessage: String) {
    AlertDialog.Builder(requireContext())
        .setTitle(R.string.title_alert_dialog)
        .setMessage(errorMessage)
        .setPositiveButton(R.string.button_close, null)
        .show()
}