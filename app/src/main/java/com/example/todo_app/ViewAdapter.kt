package com.example.todo_app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item.view.*

class ViewAdapter(private val itemClickListener: (TodoModel) -> Unit) :
    RecyclerView.Adapter<ViewAdapter.RecyclerViewHolder>() {

    var todoList: List<TodoModel> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemview = inflater.inflate(R.layout.list_item, parent, false)
        return RecyclerViewHolder(itemview)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val todo = todoList[position]
        holder.itemView.text_row_title.text = todo.title

        holder.itemView.setOnClickListener {
            itemClickListener.invoke(todo)
        }
    }

    override fun getItemCount(): Int {
        return todoList.size
    }

    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
