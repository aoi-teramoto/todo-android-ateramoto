package com.example.todo_app

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TodoListFragment : Fragment() {

    private lateinit var adapter: ViewAdapter
    private var isDeleteMode = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.observeEvent(this) {
            Snackbar.make(requireView(), it, Snackbar.LENGTH_LONG).show()
        }
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layout = LinearLayoutManager(activity)

        adapter = ViewAdapter(::itemClickListener)

        recycler_list.also {
            it.layoutManager = LinearLayoutManager(activity)
            it.adapter = adapter
            it.addItemDecoration(DividerItemDecoration(activity, layout.orientation))
        }

        CoroutineScope(Default).launch(IO) {
            fetchTodoList()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_todo_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_delete -> {
                isDeleteMode = !isDeleteMode
                val icon = if (isDeleteMode) R.drawable.ic_check else R.drawable.ic_delete
                item.setIcon(icon)
            }
            R.id.menu_create -> goEditFragment()
        }
        return super.onOptionsItemSelected(item)
    }

    private suspend fun fetchTodoList() {
        try {
            val response = ApiClient().apiRequest.fetchTodo().execute()
            withContext(Main) {
                if (response.isSuccessful) {
                    adapter.todoList = response.body()?.todos!!
                } else {
                    val gson = GsonUtils.generateGson()
                    val body =
                        gson.fromJson(response.errorBody()?.string(), CommonResponse::class.java)
                    showErrorAlertDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorAlertDialog(getString(R.string.exception_error_alert_message))
            }
        }
    }

    private suspend fun deleteTodo(id: Int) {
        try {
            val response = ApiClient().apiRequest.deleteTodo(id).execute()
            withContext(Main) {
                if (response.isSuccessful) {
                    CoroutineScope(IO).launch {
                        fetchTodoList()
                    }
                } else {
                    val gson = GsonUtils.generateGson()
                    val body =
                        gson.fromJson(response.errorBody()?.string(), CommonResponse::class.java)
                    showErrorAlertDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorAlertDialog(getString(R.string.exception_error_alert_message))
            }
        }
    }

    private fun itemClickListener(todo: TodoModel) {
        if (isDeleteMode) {
            showDeleteConfirmAlertDialog(todo.id, todo.title)
        } else {
            goEditFragment(todo)
        }
    }

    private fun showDeleteConfirmAlertDialog(id: Int, title: String) {
        AlertDialog.Builder(requireContext())
            .setMessage(getString(R.string.delete_confirmation_message, title))
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                CoroutineScope(IO).launch {
                    deleteTodo(id)
                }
            }
            .show()
    }

    private fun goEditFragment(todo: TodoModel? = null) {
        val action = TodoListFragmentDirections.actionTodoListFragmentToTodoEditFragment(todo)
        findNavController().navigate(action)
    }
}