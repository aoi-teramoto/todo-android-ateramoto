package com.example.todo_app

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetTodosResponse(
    val todos: List<TodoModel>? = null,
    val errorCode: Int,
    val errorMessage: String
) : Parcelable