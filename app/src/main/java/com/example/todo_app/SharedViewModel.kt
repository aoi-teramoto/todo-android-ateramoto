package com.example.todo_app

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModel : ViewModel() {
    val todoEditResult: MutableLiveData<Event<String>> = MutableLiveData()
}