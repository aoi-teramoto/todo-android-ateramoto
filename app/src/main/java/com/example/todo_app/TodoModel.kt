package com.example.todo_app

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class TodoModel(
    val id: Int,
    val title: String,
    val detail: String?,
    val date: Date?
) : Parcelable